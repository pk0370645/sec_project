var menu = document.getElementsByClassName("dp-menu");
var banner = document.getElementById("banner");
console.log(menu);
function dpMenu(element) {
    if (element.className == "category fashion") {
        menu[0].style.display = "block";
    }
    else if (element.className == "category electronics") {
        menu[1].style.display = "block";
    }
    else if (element.className == "category home") {
        menu[2].style.display = "block";
    }
    else {
        menu[3].style.display = "block";
    }
}


function closeMenu(element) {
    if (element.className == "category fashion") {
        menu[0].style.display = "none";
    }
    else if (element.className == "category electronics") {
        menu[1].style.display = "none";
    }
    else if (element.className == "category home") {
        menu[2].style.display = "none";
    }
    else {
        menu[3].style.display = "none";
    }

}

const banner_lst = ["assets/banner1.webp","assets/banner2.webp","assets/banner3.webp"]

function prevBanner() {
// console.log(banner.style.getPropertyValue("background"));
banner.style.background = `url(${banner_lst[1]})`;

}
function afterBanner() {
// console.log(banner.style.getPropertyValue("background"));
banner.style.background = `url(${banner_lst[2]})`;
}
